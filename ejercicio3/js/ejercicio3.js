/*entrada por medio de prompt*/
var algin = prompt('Nota Algebre Lineal: ' );
var EOE = prompt('Nota Expresion Oral y Escrita: ');
var huma = prompt('Nota Humanistica I: ');
var iai = prompt('Nota Introducción al internet: ');
var ip = prompt('Nota Introducción a la programación: ');
var qui = prompt('Nota Quimica General: ');
var lan = prompt('Nota Redes de area local: ');
var cum = 0;/*asignamos un valor neutro*/
var uv1, uv2, uv3, uv4, uv5, uv6, uv7;
var uvini, uvreal;/*dependiendo las uv ganadas se le asignan las libres para el siguiente ciclo*/

/*Asignamos las unidades valorativas para cada materia*/
    uv1 = 3;
    uv2 = 3;
    uv3 = 3;
    uv4 = 4;
    uv5 = 4;
    uv6 = 4;
    uv7 = 4;
/*Realizamos los calculos para verificar las unidades valorativas y el promedio*/
cum = (((algin*uv1)+(EOE*uv2)+(huma*uv3)+(iai*uv4)+(ip*uv5)+(qui*uv6)+(lan*uv7))/(uvini));
uvini = (uv1+uv2+uv3+uv4+uv5+uv6+uv7);

/*calculo de las uv reales*/
if(cum<6){
    uvreal = 16;
}else if((cum >= 6) && (cum< 7)){
    uvreal = 20;
}else if((cum >= 7) && (cum < 7.5)){
    uvreal = 24;
}else{
    uvreal = 32;
}
/*En estas variables almacenamos lineas de codigo para ahorrar tiempo*/
var mensaje1 = "<tr><td>";
var mensaje2 = "</td><td>";
var mensaje3 = "</tr>"; 

/*utilizando document.write mostramos el resultado de los calculos maquetando
una tabla concatenando las variables y datos fijos*/
document.write(mensaje1 + "Algebra Lineal"                + mensaje2 +  "3" + mensaje2 +  algin + mensaje3);
document.write(mensaje1 + "Expresion Oral y Escrita"       + mensaje2 +  "3" + mensaje2 + EOE + mensaje3);
document.write(mensaje1 + "Humanistica I"                  + mensaje2 +  "3" + mensaje2 +  huma + mensaje3);
document.write(mensaje1 + "Introduccion al internet"     + mensaje2 +  "4" + mensaje2 +  iai+ mensaje3);
document.write(mensaje1 + "Introduccion a la programacion" + mensaje2 +  "4" + mensaje2 +  ip + mensaje3);
document.write(mensaje1 + "Quimica General"               + mensaje2 +  "4" + mensaje2 +  qui + mensaje3);
document.write(mensaje1 + "Redes de Area Loca"            + mensaje2 +  "4" + mensaje2 +  lan + mensaje3);
document.write(mensaje1 + "UV II ciclo:  " + uvreal + " unidades" + mensaje2 +  "UV GANADAS:  " + uvini +  mensaje2 + "CUM:  " + cum.toPrecision(2) + mensaje3);/*validamos la salida de solo dos decimales*/