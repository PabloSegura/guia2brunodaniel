/***********************************************************************
Investigue como conocer por medio de JavaScript que día de la semana es, y muestre el nombre
del día, un mensaje e imagen diferente dependiendo del día.
************************************************************************/

/*Variable que almacena la fecha del sistema*/
var fecha=new Date();
/*Extraemos solo el dia con codigo del 0 al 6*/
var dia=fecha.getDay()

/*en este array iran los dias de la semana dependiendo el valor de la variable dia*/
var dianame = new Array (7); 
  dianame[0]="Domingo";
  dianame[1]="Luns";
  dianame[2]="Martes";
  dianame[3]="Miércoles";
  dianame[4]="Jueves";
  dianame[5]="Viernes";
  dianame[6]="Sábado";
/*en estas estructuras condicionales elegimos la imagen segun el dia*/
if(dia===0){
document.write(dianame[dia]);
document.write('</p><div><img class="foto" src=../img/domingo.jpg></div>');    
}else if(dia===1){
document.write(dianame[dia]);
document.write('</p><div><img class="foto" src=../img/lunes.jpg></div>');
}else if(dia===2){
document.write(dianame[dia]);
document.write('</p><div><img class="foto" src=../img/martes.jpg></div>');
}else if(dia===3){
document.write(dianame[dia]);
document.write('</p><div><img class="foto" src=../img/miercoles.jpg></div>');
}else if(dia===4){
document.write(dianame[dia]);
document.write('</p><div><img class="foto" src=../img/jueves.jpg></div>');
}else if(dia===5){
document.write(dianame[dia]);
document.write('</p><div><img class="foto" src=../img/viernes.jpg></div>');
}else if(dia===6){
document.write(dianame[dia]);
document.write('</p><div><img class="foto" src=../img/sabado.jpg></div>');
}else{
    document.write("Error no sabemos el día,recarga la página por favor...");
}
